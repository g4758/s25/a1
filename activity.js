
// no. 2

db.fruits.aggregate([
   			{$match:{onSale:true}},
   			{ $count: "fruitsOnSale"}])

//no.3

db.fruits.aggregate([
   			{$match:{"stock":{$gt:20}}},
   			{ $count: "fruitsOnSale"}])

//no.4
db.fruits.aggregate([
	{$match: {"onSale": true}},
	{$group: {_id: null, ave_price: {$avg: "$price"}}} ])
			

//no.5
db.fruits.aggregate([
	{$match: {"onSale":true}},
	{$group: {_id:null, max_price: {$max: "$price"}}}
	])

//no.6
db.fruits.aggregate([

	{$match: {"onSale":true}},
	{$group: {_id:null, min_price: {$min: "$price"}}}
	])




